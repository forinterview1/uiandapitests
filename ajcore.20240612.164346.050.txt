---- AspectJ Properties ---
AspectJ Compiler 1.9.9.1 built on Thursday Mar 31, 2022 at 05:00:07 PDT
---- Dump Properties ---
Dump file: ajcore.20240612.164346.050.txt
Dump reason: java.lang.RuntimeException
Dump on exception: true
Dump at exit condition: abort
---- Exception Information ---
java.lang.RuntimeException: bad non-abstract method with no code: void org.codehaus.groovy.runtime.callsite.AbstractCallSite.<init>(org.codehaus.groovy.runtime.callsite.CallSiteArray, int, java.lang.String) on public class org.codehaus.groovy.runtime.callsite.AbstractCallSite
	at org.aspectj.weaver.bcel.LazyMethodGen.<init>(LazyMethodGen.java:221)
	at org.aspectj.weaver.bcel.LazyClassGen.<init>(LazyClassGen.java:360)
	at org.aspectj.weaver.bcel.BcelObjectType.getLazyClassGen(BcelObjectType.java:561)
	at org.aspectj.weaver.bcel.BcelWeaver.weave(BcelWeaver.java:1699)
	at org.aspectj.weaver.bcel.BcelWeaver.weaveWithoutDump(BcelWeaver.java:1650)
	at org.aspectj.weaver.bcel.BcelWeaver.weaveAndNotify(BcelWeaver.java:1417)
	at org.aspectj.weaver.bcel.BcelWeaver.weave(BcelWeaver.java:1192)
	at org.aspectj.weaver.tools.WeavingAdaptor.getWovenBytes(WeavingAdaptor.java:549)
	at org.aspectj.weaver.tools.WeavingAdaptor.weaveClass(WeavingAdaptor.java:385)
	at org.aspectj.weaver.loadtime.Aj.preProcess(Aj.java:115)
	at org.aspectj.weaver.loadtime.ClassPreProcessorAgentAdapter.transform(ClassPreProcessorAgentAdapter.java:51)
	at sun.instrument.TransformerManager.transform(TransformerManager.java:188)
	at sun.instrument.InstrumentationImpl.transform(InstrumentationImpl.java:428)
	at java.lang.ClassLoader.defineClass1(Native Method)
	at java.lang.ClassLoader.defineClass(ClassLoader.java:756)
	at java.lang.ClassLoader.defineClass(ClassLoader.java:635)
	at org.codehaus.groovy.reflection.SunClassLoader.define(SunClassLoader.java:105)
	at org.codehaus.groovy.runtime.callsite.GroovySunClassLoader.loadAbstract(GroovySunClassLoader.java:78)
	at org.codehaus.groovy.runtime.callsite.GroovySunClassLoader.<init>(GroovySunClassLoader.java:59)
	at org.codehaus.groovy.runtime.callsite.GroovySunClassLoader.<init>(GroovySunClassLoader.java:54)
	at org.codehaus.groovy.runtime.callsite.GroovySunClassLoader.lambda$static$0(GroovySunClassLoader.java:40)
	at java.security.AccessController.doPrivileged(Native Method)
	at org.codehaus.groovy.runtime.callsite.GroovySunClassLoader.doPrivileged(GroovySunClassLoader.java:50)
	at org.codehaus.groovy.runtime.callsite.GroovySunClassLoader.<clinit>(GroovySunClassLoader.java:37)
	at org.codehaus.groovy.runtime.callsite.CallSiteGenerator.isCompilable(CallSiteGenerator.java:252)
	at org.codehaus.groovy.reflection.CachedMethod.createStaticMetaMethodSite(CachedMethod.java:302)
	at org.codehaus.groovy.runtime.callsite.StaticMetaMethodSite.createStaticMetaMethodSite(StaticMetaMethodSite.java:114)
	at groovy.lang.MetaClassImpl.createStaticSite(MetaClassImpl.java:3586)
	at org.codehaus.groovy.runtime.callsite.CallSiteArray.createCallStaticSite(CallSiteArray.java:72)
	at org.codehaus.groovy.runtime.callsite.CallSiteArray.createCallSite(CallSiteArray.java:159)
	at org.codehaus.groovy.runtime.callsite.CallSiteArray.defaultCall(CallSiteArray.java:45)
	at org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:125)
	at org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:130)
	at io.restassured.internal.common.assertion.AssertParameter.notNull(AssertParameter.groovy:21)
	at io.restassured.config.SSLConfig.<init>(SSLConfig.java:201)
	at io.restassured.config.SSLConfig.<init>(SSLConfig.java:193)
	at io.restassured.config.RestAssuredConfig.<init>(RestAssuredConfig.java:41)
	at io.restassured.RestAssured.<clinit>(RestAssured.java:422)
	at apiTests.TestApi.seti(TestApi.java:29)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at org.junit.platform.commons.util.ReflectionUtils.invokeMethod(ReflectionUtils.java:727)
	at org.junit.jupiter.engine.execution.MethodInvocation.proceed(MethodInvocation.java:60)
	at org.junit.jupiter.engine.execution.InvocationInterceptorChain$ValidatingInvocation.proceed(InvocationInterceptorChain.java:131)
	at io.qameta.allure.junit5.AllureJunit5.processFixture(AllureJunit5.java:138)
	at io.qameta.allure.junit5.AllureJunit5.interceptBeforeAllMethod(AllureJunit5.java:100)
	at org.junit.jupiter.engine.execution.InterceptingExecutableInvoker$ReflectiveInterceptorCall.lambda$ofVoidMethod$0(InterceptingExecutableInvoker.java:103)
	at org.junit.jupiter.engine.execution.InterceptingExecutableInvoker.lambda$invoke$0(InterceptingExecutableInvoker.java:93)
	at org.junit.jupiter.engine.execution.InvocationInterceptorChain$InterceptedInvocation.proceed(InvocationInterceptorChain.java:106)
	at org.junit.jupiter.engine.extension.TimeoutExtension.intercept(TimeoutExtension.java:156)
	at org.junit.jupiter.engine.extension.TimeoutExtension.interceptLifecycleMethod(TimeoutExtension.java:128)
	at org.junit.jupiter.engine.extension.TimeoutExtension.interceptBeforeAllMethod(TimeoutExtension.java:70)
	at org.junit.jupiter.engine.execution.InterceptingExecutableInvoker$ReflectiveInterceptorCall.lambda$ofVoidMethod$0(InterceptingExecutableInvoker.java:103)
	at org.junit.jupiter.engine.execution.InterceptingExecutableInvoker.lambda$invoke$0(InterceptingExecutableInvoker.java:93)
	at org.junit.jupiter.engine.execution.InvocationInterceptorChain$InterceptedInvocation.proceed(InvocationInterceptorChain.java:106)
	at org.junit.jupiter.engine.execution.InvocationInterceptorChain.proceed(InvocationInterceptorChain.java:64)
	at org.junit.jupiter.engine.execution.InvocationInterceptorChain.chainAndInvoke(InvocationInterceptorChain.java:45)
	at org.junit.jupiter.engine.execution.InvocationInterceptorChain.invoke(InvocationInterceptorChain.java:37)
	at org.junit.jupiter.engine.execution.InterceptingExecutableInvoker.invoke(InterceptingExecutableInvoker.java:92)
	at org.junit.jupiter.engine.execution.InterceptingExecutableInvoker.invoke(InterceptingExecutableInvoker.java:86)
	at org.junit.jupiter.engine.descriptor.ClassBasedTestDescriptor.lambda$invokeBeforeAllMethods$13(ClassBasedTestDescriptor.java:411)
	at org.junit.platform.engine.support.hierarchical.ThrowableCollector.execute(ThrowableCollector.java:73)
	at org.junit.jupiter.engine.descriptor.ClassBasedTestDescriptor.invokeBeforeAllMethods(ClassBasedTestDescriptor.java:409)
	at org.junit.jupiter.engine.descriptor.ClassBasedTestDescriptor.before(ClassBasedTestDescriptor.java:215)
	at org.junit.jupiter.engine.descriptor.ClassBasedTestDescriptor.before(ClassBasedTestDescriptor.java:84)
	at org.junit.platform.engine.support.hierarchical.NodeTestTask.lambda$executeRecursively$6(NodeTestTask.java:148)
	at org.junit.platform.engine.support.hierarchical.ThrowableCollector.execute(ThrowableCollector.java:73)
	at org.junit.platform.engine.support.hierarchical.NodeTestTask.lambda$executeRecursively$8(NodeTestTask.java:141)
	at org.junit.platform.engine.support.hierarchical.Node.around(Node.java:137)
	at org.junit.platform.engine.support.hierarchical.NodeTestTask.lambda$executeRecursively$9(NodeTestTask.java:139)
	at org.junit.platform.engine.support.hierarchical.ThrowableCollector.execute(ThrowableCollector.java:73)
	at org.junit.platform.engine.support.hierarchical.NodeTestTask.executeRecursively(NodeTestTask.java:138)
	at org.junit.platform.engine.support.hierarchical.NodeTestTask.execute(NodeTestTask.java:95)
	at java.util.ArrayList.forEach(ArrayList.java:1259)
	at org.junit.platform.engine.support.hierarchical.SameThreadHierarchicalTestExecutorService.invokeAll(SameThreadHierarchicalTestExecutorService.java:41)
	at org.junit.platform.engine.support.hierarchical.NodeTestTask.lambda$executeRecursively$6(NodeTestTask.java:155)
	at org.junit.platform.engine.support.hierarchical.ThrowableCollector.execute(ThrowableCollector.java:73)
	at org.junit.platform.engine.support.hierarchical.NodeTestTask.lambda$executeRecursively$8(NodeTestTask.java:141)
	at org.junit.platform.engine.support.hierarchical.Node.around(Node.java:137)
	at org.junit.platform.engine.support.hierarchical.NodeTestTask.lambda$executeRecursively$9(NodeTestTask.java:139)
	at org.junit.platform.engine.support.hierarchical.ThrowableCollector.execute(ThrowableCollector.java:73)
	at org.junit.platform.engine.support.hierarchical.NodeTestTask.executeRecursively(NodeTestTask.java:138)
	at org.junit.platform.engine.support.hierarchical.NodeTestTask.execute(NodeTestTask.java:95)
	at org.junit.platform.engine.support.hierarchical.SameThreadHierarchicalTestExecutorService.submit(SameThreadHierarchicalTestExecutorService.java:35)
	at org.junit.platform.engine.support.hierarchical.HierarchicalTestExecutor.execute(HierarchicalTestExecutor.java:57)
	at org.junit.platform.engine.support.hierarchical.HierarchicalTestEngine.execute(HierarchicalTestEngine.java:54)
	at org.junit.platform.launcher.core.EngineExecutionOrchestrator.execute(EngineExecutionOrchestrator.java:147)
	at org.junit.platform.launcher.core.EngineExecutionOrchestrator.execute(EngineExecutionOrchestrator.java:127)
	at org.junit.platform.launcher.core.EngineExecutionOrchestrator.execute(EngineExecutionOrchestrator.java:90)
	at org.junit.platform.launcher.core.EngineExecutionOrchestrator.lambda$execute$0(EngineExecutionOrchestrator.java:55)
	at org.junit.platform.launcher.core.EngineExecutionOrchestrator.withInterceptedStreams(EngineExecutionOrchestrator.java:102)
	at org.junit.platform.launcher.core.EngineExecutionOrchestrator.execute(EngineExecutionOrchestrator.java:54)
	at org.junit.platform.launcher.core.DefaultLauncher.execute(DefaultLauncher.java:114)
	at org.junit.platform.launcher.core.DefaultLauncher.execute(DefaultLauncher.java:86)
	at org.junit.platform.launcher.core.DefaultLauncherSession$DelegatingLauncher.execute(DefaultLauncherSession.java:86)
	at org.gradle.api.internal.tasks.testing.junitplatform.JUnitPlatformTestClassProcessor$CollectAllTestClassesExecutor.processAllTestClasses(JUnitPlatformTestClassProcessor.java:119)
	at org.gradle.api.internal.tasks.testing.junitplatform.JUnitPlatformTestClassProcessor$CollectAllTestClassesExecutor.access$000(JUnitPlatformTestClassProcessor.java:94)
	at org.gradle.api.internal.tasks.testing.junitplatform.JUnitPlatformTestClassProcessor.stop(JUnitPlatformTestClassProcessor.java:89)
	at org.gradle.api.internal.tasks.testing.SuiteTestClassProcessor.stop(SuiteTestClassProcessor.java:62)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at org.gradle.internal.dispatch.ReflectionDispatch.dispatch(ReflectionDispatch.java:36)
	at org.gradle.internal.dispatch.ReflectionDispatch.dispatch(ReflectionDispatch.java:24)
	at org.gradle.internal.dispatch.ContextClassLoaderDispatch.dispatch(ContextClassLoaderDispatch.java:33)
	at org.gradle.internal.dispatch.ProxyDispatchAdapter$DispatchingInvocationHandler.invoke(ProxyDispatchAdapter.java:94)
	at com.sun.proxy.$Proxy2.stop(Unknown Source)
	at org.gradle.api.internal.tasks.testing.worker.TestWorker$3.run(TestWorker.java:193)
	at org.gradle.api.internal.tasks.testing.worker.TestWorker.executeAndMaintainThreadName(TestWorker.java:129)
	at org.gradle.api.internal.tasks.testing.worker.TestWorker.execute(TestWorker.java:100)
	at org.gradle.api.internal.tasks.testing.worker.TestWorker.execute(TestWorker.java:60)
	at org.gradle.process.internal.worker.child.ActionExecutionWorker.execute(ActionExecutionWorker.java:56)
	at org.gradle.process.internal.worker.child.SystemApplicationClassLoaderWorker.call(SystemApplicationClassLoaderWorker.java:113)
	at org.gradle.process.internal.worker.child.SystemApplicationClassLoaderWorker.call(SystemApplicationClassLoaderWorker.java:65)
	at worker.org.gradle.process.internal.worker.GradleWorkerMain.run(GradleWorkerMain.java:69)
	at worker.org.gradle.process.internal.worker.GradleWorkerMain.main(GradleWorkerMain.java:74)
---- System Properties ---
java.runtime.name=OpenJDK Runtime Environment
org.gradle.internal.worker.tmpdir=C:\Users\oybek.nurmatov\IdeaProjects\ForAnInterview\build\tmp\test\work
sun.boot.library.path=C:\Users\oybek.nurmatov\.jdks\corretto-1.8.0_302\jre\bin
java.vm.version=25.302-b08
java.vm.vendor=Amazon.com Inc.
java.vendor.url=https://aws.amazon.com/corretto/
path.separator=;
java.vm.name=OpenJDK 64-Bit Server VM
file.encoding.pkg=sun.io
user.script=
sun.java.launcher=SUN_STANDARD
user.country=RU
sun.os.patch.level=
java.vm.specification.name=Java Virtual Machine Specification
user.dir=C:\Users\oybek.nurmatov\IdeaProjects\ForAnInterview
java.runtime.version=1.8.0_302-b08
java.awt.graphicsenv=sun.awt.Win32GraphicsEnvironment
java.endorsed.dirs=C:\Users\oybek.nurmatov\.jdks\corretto-1.8.0_302\jre\lib\endorsed
os.arch=amd64
java.io.tmpdir=C:\Users\OYBEK~1.NUR\AppData\Local\Temp\
line.separator=

java.vm.specification.vendor=Oracle Corporation
user.variant=
os.name=Windows 10
allure.results.directory=C:\Users\oybek.nurmatov\IdeaProjects\ForAnInterview\build\allure-results
sun.jnu.encoding=Cp1251
java.library.path=C:\Users\oybek.nurmatov\.jdks\corretto-1.8.0_302\bin;C:\Windows\Sun\Java\bin;C:\Windows\system32;C:\Windows;C:\Program Files\Common Files\Oracle\Java\javapath;C:\Windows\system32;C:\Windows;C:\Windows\System32\Wbem;C:\Windows\System32\WindowsPowerShell\v1.0\;C:\Windows\System32\OpenSSH\;C:\Program Files (x86)\Intel\Intel(R) Management Engine Components\DAL;C:\Program Files\Intel\Intel(R) Management Engine Components\DAL;C:\Program Files\Java\jdk-16.0.1;C:\Program Files\Java\jdk-19\bin;C:\Program Files\dotnet\;C:\Program Files\Docker\Docker\resources\bin;C:\Program Files\Git\cmd;C:\Program Files\Git\usr\bin;C:\Program Files\Git\usr\bin;.
java.specification.name=Java Platform API Specification
java.class.version=52.0
org.gradle.native=false
sun.management.compiler=HotSpot 64-Bit Tiered Compilers
os.version=10.0
user.home=C:\Users\oybek.nurmatov
user.timezone=Europe/Moscow
java.awt.printerjob=sun.awt.windows.WPrinterJob
file.encoding=windows-1251
java.specification.version=1.8
junit.jupiter.extensions.autodetection.enabled=true
java.class.path=C:\Users\oybek.nurmatov\IdeaProjects\ForAnInterview\build\classes\java\test;C:\Users\oybek.nurmatov\IdeaProjects\ForAnInterview\build\resources\test;C:\Users\oybek.nurmatov\IdeaProjects\ForAnInterview\build\classes\java\main;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.github.bonigarcia\webdrivermanager\5.8.0\7f80f4f267d38b9eedfd5327f142164e48939dd8\webdrivermanager-5.8.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\com.github.docker-java\docker-java\3.3.6\1a48fee9da600c8a6a76d4e7e8e74d7f85b65b74\docker-java-3.3.6.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\com.github.docker-java\docker-java-core\3.3.6\5914c06fd3c5070954abe2327dd7ab22b39ad134\docker-java-core-3.3.6.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.apache.commons\commons-compress\1.26.1\44331c1130c370e726a2e1a3e6fba6d2558ef04a\commons-compress-1.26.1.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\commons-io\commons-io\2.15.1\f11560da189ab563a5c8e351941415430e9304ea\commons-io-2.15.1.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.junit.jupiter\junit-jupiter\5.9.1\9274d3757e224bc02eae367bd481062a263c150b\junit-jupiter-5.9.1.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.junit.jupiter\junit-jupiter-params\5.9.1\ffcd1013edaeee112be11fcddeb38882d79238de\junit-jupiter-params-5.9.1.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.junit.jupiter\junit-jupiter-engine\5.9.1\1bf771097bde296c3ab174861954e8aafaaf2e94\junit-jupiter-engine-5.9.1.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.qameta.allure\allure-junit5\2.19.0\81e6b72649a4a9642d20c1311e23bcd687f81329\allure-junit5-2.19.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.qameta.allure\allure-junit-platform\2.19.0\bf7e14e12b4750532b484c1dea029ab5afd7bc2e\allure-junit-platform-2.19.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.junit.jupiter\junit-jupiter-api\5.9.1\7bb53fbc0173e9f6a9d21d58297af94b1f2f9ce1\junit-jupiter-api-5.9.1.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.junit.platform\junit-platform-launcher\1.9.1\b4534f5130dcfb10e8ac41a5fba6ec656c9ede06\junit-platform-launcher-1.9.1.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.junit.platform\junit-platform-engine\1.9.1\83591e5089d6cea5f324aa3ecca9b19d5a275803\junit-platform-engine-1.9.1.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.junit.platform\junit-platform-commons\1.9.1\3145f821b5cd10abcdc5f925baa5fffa6f1b628f\junit-platform-commons-1.9.1.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.aeonbits.owner\owner\1.0.12\c6ee88593294674c8c8ccdbd855db3f5396a4d19\owner-1.0.12.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.qameta.allure\allure-testng\2.19.0\d0393816bec562abdf2fa0ea6cc2e376124792b7\allure-testng-2.19.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.testng\testng\6.14.3\d24515dc253e77e54b73df97e1fb2eb7faf34fdd\testng-6.14.3.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.qameta.allure\allure-rest-assured\2.27.0\9e676f458a2e65fa72d840519b7f6f1c1ae528c9\allure-rest-assured-2.27.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.rest-assured\rest-assured\5.4.0\7ebfc6d591e132e7b39f13bac9280cc0e4fa4b\rest-assured-5.4.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\com.github.docker-java\docker-java-api\3.3.6\8e152880bfe595c81a25501e21a6d7b1d4df97be\docker-java-api-3.3.6.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\com.fasterxml.jackson.core\jackson-annotations\2.15.2\4724a65ac8e8d156a24898d50fd5dbd3642870b8\jackson-annotations-2.15.2.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\com.fasterxml.jackson.core\jackson-core\2.15.2\a6fe1836469a69b3ff66037c324d75fc66ef137c\jackson-core-2.15.2.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\com.fasterxml.jackson.core\jackson-databind\2.15.2\9353b021f10c307c00328f52090de2bdb4b6ff9c\jackson-databind-2.15.2.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\com.github.javafaker\javafaker\1.0.2\be0ff271b1208416822db4438864df47f90af92f\javafaker-1.0.2.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.seleniumhq.selenium\selenium-java\4.13.0\18cef0adbc08d152da07b234a04f6e8240ce215f\selenium-java-4.13.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\com.beust\jcommander\1.72\6375e521c1e11d6563d4f25a07ce124ccf8cd171\jcommander-1.72.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.apache-extras.beanshell\bsh\2.0b6\fb418f9b33a0b951e9a2978b4b6ee93b2707e72f\bsh-2.0b6.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.rest-assured\xml-path\5.4.0\928041ca2ad08c45ce8008f0cf388297bdc06efe\xml-path-5.4.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.rest-assured\json-path\5.4.0\83ed73b55e45300209493294ddc09ca5f61c46b6\json-path-5.4.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.apache.groovy\groovy-json\4.0.16\a1033674f0502962728eec12c9536ef827736fdb\groovy-json-4.0.16.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.apache.groovy\groovy-xml\4.0.16\bcdc17895f0125721ff11d9a051b2fa77f2b262e\groovy-xml-4.0.16.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.rest-assured\rest-assured-common\5.4.0\bdb5a25ba37cade60641ce76f9dccbaa08e22ba3\rest-assured-common-5.4.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.apache.groovy\groovy\4.0.16\4b23ab8f542bbbb517061cf5a0d401b632de9832\groovy-4.0.16.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.apache.httpcomponents\httpmime\4.5.13\efc110bad4a0d45cda7858e6beee1d8a8313da5a\httpmime-4.5.13.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.apache.httpcomponents\httpclient\4.5.13\e5f6cae5ca7ecaac1ec2827a9e2d65ae2869cada\httpclient-4.5.13.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.hamcrest\hamcrest\2.2\1820c0968dba3a11a1b30669bb1f01978a91dedc\hamcrest-2.2.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.ccil.cowan.tagsoup\tagsoup\1.2.1\5584627487e984c03456266d3f8802eb85a9ce97\tagsoup-1.2.1.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.qameta.allure\allure-attachments\2.27.0\25f3344258f85be5f0d4348efa165b7696c0df54\allure-attachments-2.27.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.apache.commons\commons-lang3\3.14.0\1ed471194b02f2c6cb734a0cd6f6f107c673afae\commons-lang3-3.14.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.yaml\snakeyaml\1.23\e8520d52edca3d8dc0f366cff520fb4498ec8a5f\snakeyaml-1.23-android.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\com.github.mifmif\generex\1.0.2\b378f873b4e8d7616c3d920e2132cb1c87679600\generex-1.0.2.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.seleniumhq.selenium\selenium-chrome-driver\4.13.0\490603c4c79f938ffaf30058a675d850d9717265\selenium-chrome-driver-4.13.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.seleniumhq.selenium\selenium-devtools-v115\4.13.0\76c322f8b51ce2176c1b9aae458e13d18aa51276\selenium-devtools-v115-4.13.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.seleniumhq.selenium\selenium-devtools-v116\4.13.0\5bb40cd24bcaf9d6015eb6cdf76784506d36c033\selenium-devtools-v116-4.13.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.seleniumhq.selenium\selenium-devtools-v117\4.13.0\86db93ca74312d7795df726d0412cb350c31f15b\selenium-devtools-v117-4.13.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.seleniumhq.selenium\selenium-firefox-driver\4.13.0\2f2eb0ff81797444908024dd05b36fe287204cfe\selenium-firefox-driver-4.13.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.seleniumhq.selenium\selenium-devtools-v85\4.13.0\5469963e082f59946ec627114f7baa2fa5bf81d8\selenium-devtools-v85-4.13.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.seleniumhq.selenium\selenium-edge-driver\4.13.0\5f818b6057c9a9abc3b4ac9c1b9d5dd5e478d753\selenium-edge-driver-4.13.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.seleniumhq.selenium\selenium-ie-driver\4.13.0\67137653d0d2e0c4f6ea735c9e57772ed9f5b3f4\selenium-ie-driver-4.13.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.seleniumhq.selenium\selenium-safari-driver\4.13.0\739a5e2a88b640ee7660779ece1fa0f8cfaa1372\selenium-safari-driver-4.13.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.seleniumhq.selenium\selenium-support\4.13.0\decde2a7df52ce78132dc6fe6085384327c844f0\selenium-support-4.13.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.seleniumhq.selenium\selenium-chromium-driver\4.13.0\4ff91ea6a6b7157e44490269664419cc1a008f64\selenium-chromium-driver-4.13.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.seleniumhq.selenium\selenium-remote-driver\4.13.0\5d9f84d7d51174a54fa507b1418db3b1feddec6a\selenium-remote-driver-4.13.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.seleniumhq.selenium\selenium-manager\4.13.0\d474cc621720e47e9f00ef7f4fec8cc76643d41b\selenium-manager-4.13.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.seleniumhq.selenium\selenium-http\4.13.0\351d1c150e47cf863027020fef3b9f4f38a6c99c\selenium-http-4.13.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.seleniumhq.selenium\selenium-json\4.13.0\ebab8a0a864a9715df77b8c615d8c41709282924\selenium-json-4.13.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.seleniumhq.selenium\selenium-os\4.13.0\91ce811433b76a85ff37e8b3aeaf2b919f4e65ff\selenium-os-4.13.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.seleniumhq.selenium\selenium-api\4.13.0\d0471521e3618fad0afa585103ba9f07c899b3ca\selenium-api-4.13.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\com.github.docker-java\docker-java-transport-httpclient5\3.3.6\4ab63a97382b2d1db3144512f2a182cad6a1713e\docker-java-transport-httpclient5-3.3.6.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.apache.httpcomponents.client5\httpclient5\5.2.1\c900514d3446d9ce5d9dbd90c21192048125440\httpclient5-5.2.1.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.qameta.allure\allure-test-filter\2.19.0\2c1c47f8df4186ac51f5e4384c31d438d7c9fc73\allure-test-filter-2.19.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.qameta.allure\allure-java-commons\2.27.0\d39d1dc473bfd2a2e9ddb8685672297f6553cb69\allure-java-commons-2.27.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.asynchttpclient\async-http-client\2.12.3\6dfc91814cc8b3bc3327246d0e5df36911b9a623\async-http-client-2.12.3.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.slf4j\jcl-over-slf4j\1.7.30\cd92524ea19d27e5b94ecd251e1af729cffdfe15\jcl-over-slf4j-1.7.30.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.asynchttpclient\async-http-client-netty-utils\2.12.3\ad99d8622931ed31367d0fef7fa17eb62e033fb3\async-http-client-netty-utils-2.12.3.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.slf4j\slf4j-api\2.0.12\48f109a2a6d8f446c794f3e3fa0d86df0cdfa312\slf4j-api-2.0.12.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\com.google.code.gson\gson\2.10.1\b3add478d4382b78ea20b1671390a858002feb6c\gson-2.10.1.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.brotli\dec\0.1.2\c26a897ae0d524809eef1c786cc6183b4ddcc3b\dec-0.1.2.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.opentest4j\opentest4j\1.2.0\28c11eb91f9b6d8e200631d46e20a7f407f2a046\opentest4j-1.2.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.apache.httpcomponents\httpcore\4.4.13\853b96d3afbb7bf8cc303fe27ee96836a10c1834\httpcore-4.4.13.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\commons-logging\commons-logging\1.2\4bfc12adfe4842bf07b657f0369c4cb522955686\commons-logging-1.2.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\commons-codec\commons-codec\1.16.1\47bd4d333fba53406f6c6c51884ddbca435c8862\commons-codec-1.16.1.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.freemarker\freemarker\2.3.31\cd4fc0942b4a8bdb19f3b669aa42136fb54feb55\freemarker-2.3.31.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\dk.brics.automaton\automaton\1.11-8\6ebfa65eb431ff4b715a23be7a750cbc4cc96d0f\automaton-1.11-8.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\com.google.auto.service\auto-service-annotations\1.1.1\da12a15cd058ba90a0ff55357fb521161af4736d\auto-service-annotations-1.1.1.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\com.google.guava\guava\32.1.2-jre\5e64ec7e056456bef3a4bc4c6fdaef71e8ab6318\guava-32.1.2-jre.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.netty\netty-handler-proxy\4.1.60.Final\2352f12826400e5db64b36fd951508ce9a61c196\netty-handler-proxy-4.1.60.Final.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.netty\netty-codec-http\4.1.96.Final\a4d0d95df5026965c454902ef3d6d84b81f89626\netty-codec-http-4.1.96.Final.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.netty\netty-transport-native-epoll\4.1.96.Final\9faf365396c933f1b39b60d129391c6c6c43fb86\netty-transport-native-epoll-4.1.96.Final.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.netty\netty-transport-native-epoll\4.1.96.Final\3f8904e072cfc9a8d67c6fe567c39bcbce5c9c55\netty-transport-native-epoll-4.1.96.Final-linux-x86_64.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.netty\netty-transport-classes-epoll\4.1.96.Final\b0369501645f6e71f89ff7f77b5c5f52510a2e31\netty-transport-classes-epoll-4.1.96.Final.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.netty\netty-transport-native-kqueue\4.1.96.Final\2721cf6fe8752168dafdd4187ae097e6cf9dd9f5\netty-transport-native-kqueue-4.1.96.Final.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.netty\netty-transport-native-kqueue\4.1.96.Final\c127ed313fc80cf2cb366dccfded1daddc89a8ef\netty-transport-native-kqueue-4.1.96.Final-osx-x86_64.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.netty\netty-transport-classes-kqueue\4.1.96.Final\782f6bbb8dd5401599d272ea0fb81d1356bdffb2\netty-transport-classes-kqueue-4.1.96.Final.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\com.typesafe.netty\netty-reactive-streams\2.0.4\f77c8eaa7d5e2f2160b6d21ba385cf726f164b2\netty-reactive-streams-2.0.4.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.netty\netty-handler\4.1.96.Final\7840d7523d709e02961b647546f9d9dde1699306\netty-handler-4.1.96.Final.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.netty\netty-transport-native-unix-common\4.1.96.Final\daf8578cade63a01525ee9d70371fa78e6e91094\netty-transport-native-unix-common-4.1.96.Final.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.netty\netty-codec-socks\4.1.60.Final\6f4573281df659265bd709fd10471c3e00ef6c70\netty-codec-socks-4.1.60.Final.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.netty\netty-codec\4.1.96.Final\9cfe430f8b14e7ba86969d8e1126aa0aae4d18f0\netty-codec-4.1.96.Final.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.netty\netty-transport\4.1.96.Final\dbd15ca244be28e1a98ed29b9d755edbfa737e02\netty-transport-4.1.96.Final.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.netty\netty-buffer\4.1.96.Final\4b80fffbe77485b457bf844289bf1801f61b9e91\netty-buffer-4.1.96.Final.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.netty\netty-resolver\4.1.96.Final\e51db5568a881e0f9b013b35617c597dc32f130\netty-resolver-4.1.96.Final.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.netty\netty-common\4.1.96.Final\d10c167623cbc471753f950846df241d1021655c\netty-common-4.1.96.Final.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.opentelemetry\opentelemetry-exporter-logging\1.28.0\e6721fd80fe703a9bbaf8fcdf269aa878a2fa963\opentelemetry-exporter-logging-1.28.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.opentelemetry\opentelemetry-sdk-extension-autoconfigure\1.28.0\6db66c77ca29a8d05227324a8392b736744bbe3f\opentelemetry-sdk-extension-autoconfigure-1.28.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.opentelemetry\opentelemetry-sdk-extension-autoconfigure-spi\1.28.0\582ce034be1262aac6d77b92ec2d6cf4884cee4d\opentelemetry-sdk-extension-autoconfigure-spi-1.28.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.opentelemetry\opentelemetry-sdk\1.28.0\a1ca3938a03e5bb0749dc92da91edf76f6ee3b7f\opentelemetry-sdk-1.28.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.opentelemetry\opentelemetry-sdk-trace\1.28.0\18797986d45940d873430023280211d6990680c\opentelemetry-sdk-trace-1.28.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.opentelemetry\opentelemetry-sdk-metrics\1.28.0\4d955fb6c2ec89b4f55d88d7aed4dd9c36809235\opentelemetry-sdk-metrics-1.28.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.opentelemetry\opentelemetry-sdk-logs\1.28.0\adcd0bc96bc77152a15b3b8890bd8a04dd0bf36b\opentelemetry-sdk-logs-1.28.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.opentelemetry\opentelemetry-sdk-common\1.28.0\b25e52ef6829bb41db3227d8fcc206009b018f40\opentelemetry-sdk-common-1.28.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.opentelemetry\opentelemetry-semconv\1.28.0-alpha\97336840db7cb0ef7e5d292f7cec5bdb385cc370\opentelemetry-semconv-1.28.0-alpha.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.opentelemetry\opentelemetry-api-events\1.28.0-alpha\c663155d53746308c8d2e37121b990c3a8de8d58\opentelemetry-api-events-1.28.0-alpha.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.opentelemetry\opentelemetry-extension-incubator\1.28.0-alpha\ad51d2ab228f522448afb90503ce005ce4a4f2ac\opentelemetry-extension-incubator-1.28.0-alpha.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.opentelemetry\opentelemetry-api\1.28.0\ebdea4fbe23c3929f1702b176d2cd63ac6288f0\opentelemetry-api-1.28.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.opentelemetry\opentelemetry-context\1.28.0\468c460d80c5a6e0fdddd3c1a83148b316571c22\opentelemetry-context-1.28.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\net.bytebuddy\byte-buddy\1.14.5\28a424c0c4f362568e904d992c239c996cf7adc7\byte-buddy-1.14.5.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\com.github.docker-java\docker-java-transport\3.3.6\d536d16a297f9139b833955390a3d581e336e67\docker-java-transport-3.3.6.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\net.java.dev.jna\jna\5.13.0\1200e7ebeedbe0d10062093f32925a912020e747\jna-5.13.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.apache.httpcomponents.core5\httpcore5-h2\5.2\698bd8c759ccc7fd7398f3179ff45d0e5a7ccc16\httpcore5-h2-5.2.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.apache.httpcomponents.core5\httpcore5\5.2\ab7d251b8dfa3f2878f1eefbcca0e1fc0ebeba27\httpcore5-5.2.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\io.qameta.allure\allure-model\2.27.0\f790edd579a00469065b1bf00a5184a736eef1d7\allure-model-2.27.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\com.google.guava\failureaccess\1.0.1\1dcf1de382a0bf95a3d8b0849546c88bac1292c9\failureaccess-1.0.1.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\com.google.guava\listenablefuture\9999.0-empty-to-avoid-conflict-with-guava\b421526c5f297295adef1c886e5246c39d4ac629\listenablefuture-9999.0-empty-to-avoid-conflict-with-guava.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\com.google.code.findbugs\jsr305\3.0.2\25ea2e8b0c338a877313bd4672d3fe056ea78f0d\jsr305-3.0.2.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.checkerframework\checker-qual\3.33.0\de2b60b62da487644fc11f734e73c8b0b431238f\checker-qual-3.33.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\com.google.errorprone\error_prone_annotations\2.18.0\89b684257096f548fa39a7df9fdaa409d4d4df91\error_prone_annotations-2.18.0.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\dev.failsafe\failsafe\3.3.2\738a986f1f0e4b6c6a49d351dddc772d1378c5a8\failsafe-3.3.2.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.reactivestreams\reactive-streams\1.0.3\d9fb7a7926ffa635b3dcaa5049fb2bfa25b3e7d0\reactive-streams-1.0.3.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\com.sun.activation\jakarta.activation\1.2.2\74548703f9851017ce2f556066659438019e7eb5\jakarta.activation-1.2.2.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.apache.commons\commons-exec\1.3\8dfb9facd0830a27b1b5f29f84593f0aeee7773b\commons-exec-1.3.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.bouncycastle\bcpkix-jdk18on\1.76\10c9cf5c1b4d64abeda28ee32fbade3b74373622\bcpkix-jdk18on-1.76.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.bouncycastle\bcutil-jdk18on\1.76\8c7594e651a278bcde18e038d8ab55b1f97f4d31\bcutil-jdk18on-1.76.jar;C:\Users\oybek.nurmatov\.gradle\caches\modules-2\files-2.1\org.bouncycastle\bcprov-jdk18on\1.76\3a785d0b41806865ad7e311162bfa3fa60b3965b\bcprov-jdk18on-1.76.jar
user.name=oybek.nurmatov
java.vm.specification.version=1.8
sun.java.command=worker.org.gradle.process.internal.worker.GradleWorkerMain 'Gradle Test Executor 9'
java.home=C:\Users\oybek.nurmatov\.jdks\corretto-1.8.0_302\jre
sun.arch.data.model=64
user.language=ru
java.specification.vendor=Oracle Corporation
awt.toolkit=sun.awt.windows.WToolkit
java.vm.info=mixed mode
java.version=1.8.0_302
java.ext.dirs=C:\Users\oybek.nurmatov\.jdks\corretto-1.8.0_302\jre\lib\ext;C:\Windows\Sun\Java\lib\ext
sun.boot.class.path=C:\Users\oybek.nurmatov\.jdks\corretto-1.8.0_302\jre\lib\resources.jar;C:\Users\oybek.nurmatov\.jdks\corretto-1.8.0_302\jre\lib\rt.jar;C:\Users\oybek.nurmatov\.jdks\corretto-1.8.0_302\jre\lib\sunrsasign.jar;C:\Users\oybek.nurmatov\.jdks\corretto-1.8.0_302\jre\lib\jsse.jar;C:\Users\oybek.nurmatov\.jdks\corretto-1.8.0_302\jre\lib\jce.jar;C:\Users\oybek.nurmatov\.jdks\corretto-1.8.0_302\jre\lib\charsets.jar;C:\Users\oybek.nurmatov\.jdks\corretto-1.8.0_302\jre\lib\jfr.jar;C:\Users\oybek.nurmatov\.jdks\corretto-1.8.0_302\jre\classes
java.vendor=Amazon.com Inc.
org.gradle.test.worker=9
file.separator=\
java.vendor.url.bug=https://github.com/corretto/corretto-8/issues/
sun.io.unicode.encoding=UnicodeLittle
sun.cpu.endian=little
sun.desktop=windows
sun.cpu.isalist=amd64
---- Command Line ---
Empty
---- Full Classpath ---
Empty
---- Compiler Messages ---
Empty
