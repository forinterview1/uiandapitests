package apiTests;

import apiTests.listener.CustomTpl;
import apiTests.models.faceapiusers.Address;
import apiTests.models.faceapiusers.Geolocation;
import apiTests.models.faceapiusers.Name;
import apiTests.models.faceapiusers.Response;
import io.restassured.RestAssured;
import io.restassured.common.mapper.TypeRef;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static io.restassured.RestAssured.given;

public class TestApi {
    @BeforeAll
    public static void seti(){
        RestAssured.baseURI = "https://fakestoreapi.com";
        // Здесь можно использовать: new AllureRestAssured()
        RestAssured.filters(new RequestLoggingFilter(), new ResponseLoggingFilter(),
                CustomTpl.customLogFilter().withCustomTemplates());
    }

    @Test
    @DisplayName("Проверка получения JSON")
    public void getAndRefactoringUsersData(){
        //Получим вес JSON файл
        int userId = 5;
        Response repon =  given()
                .pathParam("userId", userId)
                .get("/users/{userId}")
                .then()
                .statusCode(200)
                .extract().as(Response.class);

        //Получим только не обходимый часть JSON
        Name fio =  given()
                .pathParam("userId", userId)
                .get("/users/{userId}")
                .then()
                .statusCode(200)
                .extract()
                .jsonPath().getObject("name", Name.class);

        Assertions.assertEquals(repon.getId(), userId);
        Assertions.assertTrue(repon.getAddress().getZipcode().matches("\\d{5}-\\d{4}"));
    }

    @ParameterizedTest
    @ValueSource(ints = { 5, 10})
    @DisplayName("Проверка limit, количество пользователей")
    public void getAllUsersLimitTest(int limit){
        List<Response> users = given()
                .queryParam("limit", limit)
                .get("/users")
                .then()
                .statusCode(200)
                .extract()
                .jsonPath()
                .getList("", Response.class);

        Assertions.assertEquals(limit, users.size());
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 20})
    @DisplayName("негативное тестирования проверка limit")
    public void getAllUsersLimitErrorParamsTest(int limit){
        List<Response> users = given()
                .queryParam("limit", limit)
                .get("/users")
                .then()
                .statusCode(200)
                .extract()
                .jsonPath()
                .getList("", Response.class);

        Assertions.assertNotEquals(limit, users.size());
    }

    @Test
    @DisplayName("Сортировка и сравнения по DESC")
    public void getAllUsersSortByDESCTest() {
        String sort = "desc";
        List<Response> responseSortByDesk = given().queryParam("sort", sort)
                .get("/users")
                .then()
                .extract().as(new TypeRef<List<Response>>(){});

        List<Response> responseNotSortByDesk = given().get("/users")
                .then().extract().as(new TypeRef<List<Response>>(){});

        List<Integer> sortedResponseIds = responseSortByDesk.stream()
                .map(Response::getId).collect(Collectors.toList());
        List<Integer> notSortedResponseIds = responseNotSortByDesk.stream()
                .map(Response::getId).collect(Collectors.toList());

        List<Integer> sortedByCode = notSortedResponseIds
                .stream().sorted(Comparator.reverseOrder())
                .collect(Collectors.toList());

        Assertions.assertNotEquals(sortedResponseIds, notSortedResponseIds);
        Assertions.assertEquals(sortedByCode, sortedResponseIds);
    }

    @Test
    @DisplayName("Добавления нового пользователя")
    public void postNewUsers() {
        Random random = new Random();
        Geolocation geolocation = new Geolocation("-37.3159", "81.1496");
        Name nams = new Name("Ivan", "Ivanovich");
        Address address = Address.builder()
                .city("Moscow")
                .number(random.nextInt(300))
                .street("pushkinskiy")
                .zipcode("12546-1254")
                .geolocation(geolocation).build();

        Response response = Response
                .builder().phone("1-570-236-7033")
                .email("faceemail@mail.com")
                .address(address)
                .password("jams1985Joeji")
                .username("Jams")
                .name(nams).build();

        given().body(response).post("/users")
                .then();
    }

    private Response getUser(){
        Geolocation geolocation = new Geolocation("-37.3159", "81.1496");
        Name nams = new Name("Ivan", "Ivanovich");
        Address address = Address.builder()
                .city("Moscow")
                .number(125)
                .street("pushkinskiy")
                .zipcode("12546-1254")
                .geolocation(geolocation).build();

        return Response
                .builder().phone("1-570-236-7033")
                .email("faceemail@mail.com")
                .address(address)
                .password("jams1985Joeji")
                .username("Jams")
                .name(nams).build();
    }

    @Test
    @DisplayName("Обновляем данные пользователя")
    public void putEmailAddres(){
        Response putRquest = getUser();
        String oldPassword = putRquest.getPassword();

        putRquest.setPassword("Bolg`ali");
        Response response = given().body(putRquest)
                .put("/users/" + getUser().getId())
                .then()
                .extract().as(Response.class);

        Assertions.assertNotEquals(response.getPassword(), oldPassword);

    }
}
