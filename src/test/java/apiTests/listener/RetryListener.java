package apiTests.listener;

import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.extension.AfterTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestExecutionExceptionHandler;

import java.io.File;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;

public class RetryListener implements TestExecutionExceptionHandler, AfterTestExecutionCallback {
    private static final int MAX_RETRIES = 3;
    private static final Set<String> failedTestName = new HashSet<>();
    @Override
    public void handleTestExecutionException(ExtensionContext extensionContext, Throwable throwable) throws Throwable {

        for (int i = 0; i < MAX_RETRIES; i++) {
            try {
                extensionContext.getRequiredTestMethod().invoke(extensionContext.getRequiredTestInstance());
                return;
            }catch (Throwable ex){
                throwable = ex.getCause();
            }
        }
        throw throwable;
    }

    @Override
    public void afterTestExecution(ExtensionContext extensionContext) throws Exception {
        Method method = extensionContext.getRequiredTestMethod();
        String testClass = extensionContext.getRequiredTestClass().getName();
        String testMethod = method.getName();
        String testToWrite = String.format("--tests %s.%s", testClass, testMethod);
        extensionContext.getExecutionException().ifPresent(x->failedTestName.add(testToWrite));
    }
    @SneakyThrows
    public static void saveFailTest(){
        String output = System.getProperty("user.dir") + "/src/test/resources/FailedTest.txt";
        String result = String.join(" ", failedTestName);
        FileUtils.writeStringToFile(new File(output), result);
    }
}
