package apiTests.listener;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class RetryListenerTestNG implements IRetryAnalyzer {
    private final int MAX_RETRIES = 2;
    private int count = 0;
    @Override
    public boolean retry(ITestResult iTestResult) {
        if (count < MAX_RETRIES){
            count++;
            return true;
        }
        return false;
    }
}
