package uiTests;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class BasePage {
    protected WebDriver driver;
    protected WebDriverWait wait;
    protected JavascriptExecutor js;

    public BasePage(WebDriver driver){
        this.driver = driver;
        wait = new WebDriverWait(driver, Duration.ofSeconds(30));
        js = (JavascriptExecutor) driver;
    }
    //Метод найдет введенный город из списка
    public WebElement waitForTextPresentedInList(By list, String value){
        wait.until(ExpectedConditions.elementToBeClickable(list));
        return driver.findElements(list).stream()
                .filter(x->x.getText().contains(value))
                .findFirst().orElseThrow(()->new NoSuchElementException("Города нет " + value));
    }
    //Метод ожидает до тех пор, пока элемент становится текстом
    public void waitForTextTextMatchesRegrex(By locator, String regrex){
        Pattern pattern = Pattern.compile(regrex);
        wait.until(ExpectedConditions.textMatches(locator, pattern));
    }
    //Метод ожидает до тех пор, пока элемент пропадет
    public void waitForElementDisappear(By locator){
        wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    //Метод ожидает до тех пор, пока элемент появляется
    public void waitForElementApper(By locator){
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public Integer getDigitFromWebElement(WebElement element){
        String text = element.getText().replaceAll("[^0-9.]", "");
        return Integer.parseInt(text);
    }
    public List<Integer> getDigitsFromList(By locator){
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
        return driver.findElements(locator).stream()
                .filter(x->x.isDisplayed())
                .map(x->getDigitFromWebElement(x))
                .collect(Collectors.toList());
    }
}
