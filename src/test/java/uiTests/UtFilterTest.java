package uiTests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import uiTests.pages.MainPage;
import uiTests.pages.UtSearchPage;

import java.util.List;

public class UtFilterTest extends BaseTest{
    private final int dayForward = 24;
    private final int dayBack = 28;
    private final String cityFrom = "Москва";
    private final String cityTo = "Стамбул";

    @Test
    @DisplayName("Тест для проверка дата вылета и дата прилета в плюшке")
    public void mainPageTest(){
        UtSearchPage utSearchPage = new MainPage(driver)
                .setCityFrom(cityFrom)
                .setCityTo(cityTo)
                .setDayForward(dayForward)
                .setDayBack(dayBack)
                .search();

        int actualDayForward = utSearchPage.getMainDayForward();
        int actualDayBack = utSearchPage.getMainDayBack();
        Assertions.assertEquals(dayForward, actualDayForward);
        Assertions.assertEquals(dayBack, actualDayBack);

    }
    @Test
    @DisplayName("Тест для проверка дата вылета и дата прилета на листе варианты")
    public void filterPages(){
        UtSearchPage utSearchPage = new MainPage(driver)
                .setCityFrom(cityFrom)
                .setCityTo(cityTo)
                .setDayForward(dayForward)
                .setDayBack(dayBack)
                .search();

        List<Integer> daysForward = utSearchPage.getDaysForward();
        List<Integer> daysBack = utSearchPage.getDaysBack();

        boolean isAllDaysForwardOK = daysForward.stream().allMatch(x->x.equals(dayForward));
        boolean isAllDaysBackOK = daysBack.stream().allMatch(x->x.equals(dayBack));

        Assertions.assertTrue(isAllDaysForwardOK);
        Assertions.assertTrue(isAllDaysBackOK);
    }
}
