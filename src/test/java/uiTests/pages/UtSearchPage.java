package uiTests.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import uiTests.BasePage;

import java.util.List;

public class UtSearchPage extends BasePage {
    public UtSearchPage(WebDriver driver) {
        super(driver);
    }

    private final By titleLoader = By.xpath("//span[@class = 'countdown-title__text']");
    private final By priceDay = By.xpath("//li[@class = 'price--current']//span[@class = 'prices__price currency_font currency_font--rub']");
    private final By selectedDayForward = By.xpath("//li[@class = 'price--current']//span[@class='prices__date'][1]");
    private final By selectedDayBack = By.xpath("//li[@class = 'price--current']//span[@class='prices__date'][2]");
    private final By listOfForwardDays = By.xpath("//div[@class='ticket-details']//div[@class='ticket-action-airline-container']//following::span[@class = 'flight-brief-date__day'][1]");
    private final By listOfBackDays = By.xpath("//div[@class='ticket-details']//div[@class='ticket-action-airline-container']//following::span[@class = 'flight-brief-date__day'][3]");


    public List<Integer> getDaysForward(){
        return getDigitsFromList(listOfForwardDays);
    }
    public List<Integer> getDaysBack(){
        return getDigitsFromList(listOfBackDays);
    }

    public Integer getMainDayForward(){
        return getDigitFromWebElement(driver.findElement(selectedDayForward));
    }

    public Integer getMainDayBack(){
        return getDigitFromWebElement(driver.findElement(selectedDayBack));
    }

    public void waitForPage(){
        waitForElementApper(selectedDayForward);
        waitForTextTextMatchesRegrex(priceDay, "\\d+");
    }

    public void waitForTitleDisappear(){
        waitForElementDisappear(titleLoader);
    }
}
